import 'package:miles_converter/units.dart';

class Convertor {
  static final convertorMap = {
    Units.meters: [1, 0.001, 0, 0, 3.28084, 0.000621371, 0, 0],
    Units.kilometers: [1000, 1, 0, 0, 3280.84, 0.621371, 0, 0],
    Units.grams: [0, 0, 1, 0.0001, 0, 0, 0.00220462, 0.035274],
    Units.kilograms: [0, 0, 1000, 1, 0, 0, 2.20462, 35.274],
    Units.feet: [0.3048, 0.0003048, 0, 0, 1 , 0.000189394, 0, 0],
    Units.miles: [1609.34, 1.60934, 0, 0, 5280 , 1, 0, 0],
    Units.pounds: [0, 0, 453.592, 0.453592, 0, 0, 1, 16],
    Units.ounces: [0, 0, 28.3495, 0.0283495, 0, 0, 0.0625, 1 ],
  };

  static double convert(from, to, number) {
    var convertor = convertorMap[from]![to.index];
    return number * convertor;
  }
}
