import 'package:flutter/material.dart';

import 'convertorWidget.dart';

void main() {
  runApp(const MilesConverter());
}

class MilesConverter extends StatelessWidget {
  const MilesConverter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Miles converter',
      home: MilesConverterWidget(),
    );
  }
}
