import 'package:flutter/material.dart';
import 'package:miles_converter/convertor.dart';
import 'package:miles_converter/units.dart';

class MilesConverterWidget extends StatefulWidget {
  const MilesConverterWidget({Key? key}) : super(key: key);

  @override
  State<MilesConverterWidget> createState() => _MilesConverterWidget();
}

class _MilesConverterWidget extends State<MilesConverterWidget> {
  late double _number;
  late double _numberConverted;
  late Units _initialUnits;
  late Units _finalUnits;

  @override
  void initState() {
    super.initState();
    _number = 0.0;
    _numberConverted = 0.0;
    _initialUnits = Units.miles;
    _finalUnits = Units.meters;
  }

  void convertNumber() {
    if (_number == 0) {
      return;
    }
    setState(() {
      _numberConverted = Convertor.convert(_initialUnits, _finalUnits, _number);
    });
  }

  void handleNumberChange(String text) {
    var parsed = double.tryParse(text);
    setState(() {
      if (parsed != null) {
        _number = parsed;
      } else {
        _number = 0.0;
      }
    });
  }

  void handleInitialUnitChange(Units? text) {
    if (text == null) {
      return;
    }
    setState(() {
      _initialUnits = text;
    });
  }

  void handleFinalUnitChange(Units? text) {
    if (text == null) {
      return;
    }
    setState(() {
      _finalUnits = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle styleEntree = TextStyle(
      fontSize: 20,
      color: Colors.blue[900],
    );

    final TextStyle styleLabel = TextStyle(
      fontSize: 20,
      color: Colors.grey[700],
    );

    return Scaffold(
        appBar: AppBar(
          title: const Text('Miles converter'),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(children: <Widget>[
            const Spacer(),
            Text(
              'Value to convert',
              style: styleEntree,
            ),
            const Spacer(),
            TextField(
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(), hintText: 'Input your value'),
              onChanged: handleNumberChange,
            ),
            const Spacer(),
            Text(
              'From',
              style: styleLabel,
            ),
            const Spacer(),
            DropdownButton<Units>(
                value: _initialUnits,
                items: Units.values.map<DropdownMenuItem<Units>>((Units value) {
                  return DropdownMenuItem<Units>(
                    value: value,
                    child: Text(value.toString().split('.')[1]),
                  );
                }).toList(),
                onChanged: handleInitialUnitChange),
            const Spacer(),
            Text(
              'To',
              style: styleLabel,
            ),
            const Spacer(),
            DropdownButton<Units>(
                value: _finalUnits,
                items: Units.values.map<DropdownMenuItem<Units>>((Units value) {
                  return DropdownMenuItem<Units>(
                    value: value,
                    child: Text(value.toString().split('.')[1]),
                  );
                }).toList(),
                onChanged: handleFinalUnitChange),
            const Spacer(
              flex: 2,
            ),
            ElevatedButton(
                onPressed: convertNumber,
                child: Text(
                  'Convert',
                  style: styleEntree,
                )),
            const Spacer(
              flex: 2,
            ),
            Text(
              _numberConverted == 0
                  ? 'This conversion cannot be made'
                  : _numberConverted.toString() +
                      ' ' +
                      _finalUnits.toString().split('.')[1],
              style: styleEntree,
              textAlign: TextAlign.center,
            ),
            const Spacer(
              flex: 8,
            ),
          ]),
        ));
  }
}
