enum Units {
  meters,
  kilometers,
  grams,
  kilograms,
  feet,
  miles,
  pounds,
  ounces
}